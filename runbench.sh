#!/bin/sh

#Run benchmarks of hand-written programs. Runs 10 iterations per
#benchmark. Outputs benchmark_name time_in_us for each benchmark run

for f in CMF-seq_*; do
    for i in $(seq 1 10); do
        echo -n "${f} " 2>&1
        ./${f} 2>&1 > /dev/null | grep Time | awk '{print $6}'
    done
done

for f in CMF-orig-cuda_*; do
    for i in $(seq 1 10); do
        echo -n "${f} " 2>&1
        ./${f} 2>&1 > /dev/null | grep Time | awk '{print $6}'
    done
done
