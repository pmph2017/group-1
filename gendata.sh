#!/bin/sh

set -ex

# Generate input data for futhark and C-code
for n in 150 300 600; do
    ./img2fut/img2fut.py --iterations=$n --errorbound=0.0 --output=c images/cameraman.jpg > datasets/cameraman-$n.h
    ./img2fut/img2fut.py --iterations=$n --errorbound=0.0 --output=futhark images/cameraman.jpg > datasets/cameraman-$n.data
    ./img2fut/img2fut.py --iterations=$n --errorbound=0.0 --output=c images/scanimg.jpg > datasets/scanimg-$n.h;
    ./img2fut/img2fut.py --iterations=$n --errorbound=0.0 --output=futhark images/scanimg.jpg > datasets/scanimg-$n.data
done
