#ifndef CMF_WRAP_H
#define CMF_WRAP_H

#include <stdint.h>

typedef struct cmf_state cmf_state;

cmf_state* cmfwrap_init(float* img, int32_t rows, int32_t cols, float cc, float gradstep);
int cmfwrap_iteration(cmf_state* st, float* curimg, float* error);
void cmfwrap_free(cmf_state* st);

#endif //CMD_WRAP_H
