#include <stdlib.h>
#include <stdint.h>

#include "CMF.h"

#include "CMFwrap.h"

struct cmf_state {
  int32_t rows;
  int32_t cols;
  float cc;
  float gradstep;

  struct futhark_context_config* futctxcfg;
  struct futhark_context* futctx;

  struct futhark_f32_1d* pfdv;
  struct futhark_f32_1d* pfps;
  struct futhark_f32_1d* pfpt;
  struct futhark_f32_1d* pfbx;
  struct futhark_f32_1d* pfby;
  struct futhark_f32_1d* penalty;
  struct futhark_f32_1d* pfCs;
  struct futhark_f32_1d* pfCt;
  struct futhark_f32_1d* pfu;
};

cmf_state* cmfwrap_init(float* img, int32_t rows, int32_t cols, float cc, float gradstep) {
  cmf_state* st = calloc(1, sizeof(cmf_state));
  st->rows = rows;
  st->cols = cols;
  st->cc = cc;
  st->gradstep = gradstep;

  // Initialize Futhark
  st->futctxcfg = futhark_context_config_new();
  st->futctx = futhark_context_new(st->futctxcfg);

  // Image data to futhark array
  struct futhark_f32_1d* in_img = futhark_new_f32_1d(st->futctx,
                                                     img, rows * cols);
  futhark_init(st->futctx,
               // Output
               &st->pfdv,
               &st->pfps,
               &st->pfpt,
               &st->pfbx,
               &st->pfby,
               &st->penalty,
               &st->pfCs,
               &st->pfCt,
               &st->pfu,
               // Input
               in_img,
               rows,
               cols);

  futhark_free_f32_1d(st->futctx, in_img);

  return st;
}

int cmfwrap_iteration(cmf_state* st, float* curimg, float* error) {

  int ret;

  struct futhark_f32_1d* pfdv_tmp;
  struct futhark_f32_1d* pfps_tmp;
  struct futhark_f32_1d* pfpt_tmp;
  struct futhark_f32_1d* pfbx_tmp;
  struct futhark_f32_1d* pfby_tmp;
  struct futhark_f32_1d* penalty_tmp;
  struct futhark_f32_1d* pfCs_tmp;
  struct futhark_f32_1d* pfCt_tmp;
  struct futhark_f32_1d* pfu_tmp;

  ret = futhark_CMF(st->futctx,
                    // Output
                    error,
                    &pfdv_tmp,
                    &pfps_tmp,
                    &pfpt_tmp,
                    &pfbx_tmp,
                    &pfby_tmp,
                    &penalty_tmp,
                    &pfCs_tmp,
                    &pfCt_tmp,
                    &pfu_tmp,
                    // Input
                    st->cols,
                    st->rows,
                    st->cc,
                    st->gradstep,
                    st->pfdv,
                    st->pfps,
                    st->pfpt,
                    st->pfbx,
                    st->pfby,
                    st->penalty,
                    st->pfCs,
                    st->pfCt,
                    st->pfu
                    );

  futhark_free_f32_1d(st->futctx, st->pfdv);
  futhark_free_f32_1d(st->futctx, st->pfps);
  futhark_free_f32_1d(st->futctx, st->pfpt);
  futhark_free_f32_1d(st->futctx, st->pfbx);
  futhark_free_f32_1d(st->futctx, st->pfby);
  futhark_free_f32_1d(st->futctx, st->penalty);
  futhark_free_f32_1d(st->futctx, st->pfCs);
  futhark_free_f32_1d(st->futctx, st->pfCt);
  futhark_free_f32_1d(st->futctx, st->pfu);

  st->pfdv = pfdv_tmp;
  st->pfps = pfps_tmp;
  st->pfpt = pfpt_tmp;
  st->pfbx = pfbx_tmp;
  st->pfby = pfby_tmp;
  st->penalty = penalty_tmp;
  st->pfCs = pfCs_tmp;
  st->pfCt = pfCt_tmp;
  st->pfu = pfu_tmp;

  // Copy values to caller
  futhark_values_f32_1d(st->futctx, pfu_tmp, curimg);

  return ret;
}

void cmfwrap_free(cmf_state* st) {
  futhark_free_f32_1d(st->futctx, st->pfdv);
  futhark_free_f32_1d(st->futctx, st->pfps);
  futhark_free_f32_1d(st->futctx, st->pfpt);
  futhark_free_f32_1d(st->futctx, st->pfbx);
  futhark_free_f32_1d(st->futctx, st->pfby);
  futhark_free_f32_1d(st->futctx, st->penalty);
  futhark_free_f32_1d(st->futctx, st->pfCs);
  futhark_free_f32_1d(st->futctx, st->pfCt);
  futhark_free_f32_1d(st->futctx, st->pfu);

  futhark_context_free(st->futctx);
  futhark_context_config_free(st->futctxcfg);

  free(st);
}
