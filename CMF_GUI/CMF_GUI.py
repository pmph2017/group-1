#!/usr/bin/python3

import signal

from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtCore import Qt
from mainwindow import Ui_MainWindow

from cffi import FFI
import numpy as np
from scipy import misc
import os, sys
lib_path = os.path.abspath(os.path.join('..'))
sys.path.append(lib_path)

class CMFWrapper:
  def __init__(self, img, cc, gradstep):
    (width, height) = img.shape
    self.width = width
    self.height = height

    self.ffi = FFI()
    # This goes against "best practices" of cffi, but I think it's neat!
    with open('CMFwrap.pyffi', 'r') as f:
      self.ffi.cdef(f.read())

    self.lib = self.ffi.dlopen('./CMFwrap.so')

    img = self.ffi.cast("float *", np.reshape(img, width*height).ctypes.data)

    # These aren't really needed, but want to be explicit about type conversions
    rows = self.ffi.cast("int32_t", height)
    cols = self.ffi.cast("int32_t", width)
    cc = self.ffi.cast("float", cc)
    gradstep = self.ffi.cast("float", gradstep)

    self.cmf_state = self.lib.cmfwrap_init(img, cols, rows, cc, gradstep)

  def iteration(self):
    curimg = self.ffi.new("float [%d]" % (self.width * self.height));
    error = self.ffi.new("float *")
    self.lib.cmfwrap_iteration(self.cmf_state, curimg, error)
    arr = np.frombuffer(self.ffi.buffer(curimg, self.width * self.height * 4), np.float32)
    return (np.reshape(arr, (self.width, self.height)), float(error[0]))

  def free(self):
    self.lib.cmfwrap_free(self.cmf_state)

  def __del__(self):
    self.free()

class CMFRenderer:
  def __init__(self, ui):
    self.img = None
    self.img_size = 0
    self.img_mem = None
    self.img_w = 0
    self.img_h = 0
    self.ui = ui

    self.imagecache = []

    # Set up GUI signals
    ui.btn_Render.clicked.connect(self.cb_btnRender)
    ui.btn_Render.enabled = False
    ui.btn_SelectFile.clicked.connect(self.cb_btnSelectFile)
    ui.iCurrentFrame.valueChanged.connect(self.cb_changeImage)

  # append an image to the cache of images we have laying around
  # - will update the slider
  def cache_img(self, img_mem, iteration, error):
    self.imagecache.append((img_mem, iteration, error))
    self.ui.iCurrentFrame.setMinimum(0)                          # needed to trigger update for first image
    self.ui.iCurrentFrame.setMaximum(len(self.imagecache)-1)
    self.ui.iCurrentFrame.setValue(len(self.imagecache)-1)

  # callback for when the user yanks on the slider
  def cb_changeImage(self, index):
    if (index < len(self.imagecache)):
      self.showImageF(*self.imagecache[index])

  # callback for 'Render' button - starts the magic
  def cb_btnRender(self):
    self.maxiterations = self.ui.iIterations.value()
    self.errorbound = self.ui.iErrorbound.value()
    self.cc = self.ui.iCc.value()
    self.gradstep = self.ui.iGradstep.value()

    if self.img is None:
      self.loadFile(self.ui.sFilename.text())
    self.ui.statusBar.showMessage("Initializing...")

    del self.imagecache[1:]

    cmfwrap = CMFWrapper(self.img, self.cc, self.gradstep)

    error = 1000000
    iteration = 1
    while error > self.errorbound and iteration <= self.maxiterations:
      QApplication.processEvents()
      self.ui.statusBar.showMessage("Rendering iteration %d of %d (error = %f)..." %(iteration, self.maxiterations, error))

      (img, error) = cmfwrap.iteration()

      self.cache_img(img, iteration, error)
      iteration += 1

    # Make status text correct if we stop due to reached errorbound
    if iteration < self.maxiterations:
      self.maxiterations = iteration - 1

    self.ui.statusBar.showMessage("Done.  Iterations = %d, Final error = %f" %(len(self.imagecache)-1, error))

  def showImageF(self, img, iteration, error):
    height = img.shape[0]
    width = img.shape[1]
    imgrgb = (img*255).astype(np.uint32)

    # From: https://stackoverflow.com/questions/9794019/convert-numpy-array-to-pyside-qpixmap
    imgrgb = (255 << 24 | imgrgb[:,:] << 16 | imgrgb[:,:] << 8 | imgrgb[:,:]).flatten()

    image = QImage(imgrgb, width, height, QImage.Format_RGB32)

    pixmap = QPixmap(width, height)
    pixmap.convertFromImage(image)
    self.ui.img_Image.setPixmap(pixmap.scaled(self.ui.img_Image.width(),
                                              self.ui.img_Image.height(),
                                              Qt.KeepAspectRatio))

    self.ui.statusBar.showMessage("Showing iteration %d of %d, Error = %f" % (iteration,
                                                                              self.maxiterations,
                                                                              error))

  def loadFile(self, filename):
    print("Loading %s" % filename)
    self.img = misc.imread(filename, mode='F')
    (height, width) = self.img.shape
    self.img        = self.img / 255
    self.img_w    = np.int32(self.img.shape[0])
    self.img_h    = np.int32(self.img.shape[1])
    self.img_size = np.int32(self.img.nbytes)
    self.imagecache = []
    self.cache_img(self.img, 0, 0)
    self.ui.statusBar.showMessage("Loaded %d bytes (%dx%d)" % (self.img_size,
                                                               self.img_w, self.img_h))

  def cb_btnSelectFile(self):
    fname = QFileDialog.getOpenFileName(None, 'Open file', '.')
    if '' != fname[0]:
      self.ui.sFilename.setText(fname[0])
      self.loadFile(fname[0])

def main():
  # Fix Ctrl + C
  signal.signal(signal.SIGINT, signal.SIG_DFL)

  app = QApplication(sys.argv)
  window = QMainWindow()
  ui = Ui_MainWindow()
  ui.setupUi(window)
  cmfr = CMFRenderer(ui)
  window.show()
  sys.exit(app.exec_())

if __name__ == "__main__":
  main()
