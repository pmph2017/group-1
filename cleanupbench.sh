#!/bin/sh

rm CMF-seq_cameraman*
rm CMF-seq_scanimg*
rm CMF-orig-cuda_cameraman*
rm CMF-orig-cuda_scanimg*
rm CMF-unroll-*
rm datasets/cameraman* datasets/scanimg*
