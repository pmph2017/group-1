\documentclass[handout]{beamer}
\usetheme{metropolis}           % Use metropolis theme

%\usepackage{moresize}
\usepackage[cache=false]{minted}
\usepackage{booktabs}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{fancyvrb}

\title{Continuous Max-Flow Image Segmentation in Futhark}
\subtitle{PMPH project presentation}
\date{November 8, 2017}
\author{
    %Tycho Canter Cremers {\tt <tycho.c.c@gmail.com>} \\
    Truls Asheim  {\tt <truls@asheim.dk>}  \\
    Franck Franck {\tt <ffranck@bkultrasound.com>} }

\institute{}
\begin{document}
\maketitle
\begin{frame}{Outline}
  \begin{enumerate}
  \item Introduction and motivation
  \item Continuous Max-Flow Image Segmentation
  \item Results
  \item GUI for rapid prototyping
  \item Conclusions
  \end{enumerate}
\end{frame}
\section{Introduction and Motivation}
\begin{frame}{Motivation}
  \begin{itemize}
    \item Outcomes of examination using medical imaging devices (ultrasound in
      this case) depends on operators interpretations
    \item Intelligent image processing for processing output can help operators
      make informed decisions.
    \item Currently, special-purpose hardware is frequently used for such
      processing. Significant cost reductions if processing is performed on,
      e.g., GPGPUs.
    \item We evaluate a Futhark implementation of an algorithm of interest.
  \end{itemize}
\end{frame}
\begin{frame}{Project Objectives}
  \begin{enumerate}
    \item Evaluate if algorithm is able to achieve the performance required for
      processing images in real-time (segmentation performance = 60 fps)
    \item Compare the Futhark implementation to a hand-written CUDA
      implementation. Can this project benefit from the advantages of using a
      high-level language?
  \end{enumerate}
\end{frame}
\begin{frame}{Ultrasound example}
  \begin{figure}
    \begin{center}
      \includegraphics[width=.75\textwidth]{../Report/images/ultrasound}
      \caption{Example of an ultrasound image of a hip joint.}
    \end{center}
    \label{fig:ultrasound}
  \end{figure}
\end{frame}

\begin{frame}{Continuous Max-Flow Image Segmentation}
  \begin{itemize}
  \item Iteratively erodes away indistinct features leaving strongly connected
    and distinct features in output
  \item Able to assist medical imaging specialists by highlighting areas of
    interest
  \item Reference implementations in Matlab, Sequential C and CUDA
  \end{itemize}
\end{frame}

\begin{frame}{Continuous Max-Flow Image Segmentation}
  \begin{figure}[!h]
  \includegraphics[width=.45\textwidth]{../Report/images/cameraman} \includegraphics[width=0.45\textwidth]{../Report/images/Cameraman_Segmented}
  \caption{Reference image, before (left) and after (right) segmentation with
    the CMF algorithm}
\end{figure}
\end{frame}

\begin{frame}{Problem structure analysis}
  \begin{itemize}
  \item Algorithm structured as one main convergence loop containing several filter loops
    working on input-sized arrays
  \item Main loop not parallel: Inter-iteration dependencies
  \item Filter loops are highly parallel: Depends only on values of previous
    convergence iteration
  \item Each loop translates cleanly to a map over an input image sized list in
    Futhark
  \item Several filter maps are candidates for fusion by Futhark
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Example}
A filter loop in C:
\begin{minted}{c}
for (ix=0; ix< iNx; ix++) {
  idx = ix*iNy;
  for (iy=0; iy< iNy; iy++) {
     index = idx + iy;
     pfgk[index] = pfdv[index] - (pfps[index]
                                - pfpt[index]
                                + pfu[index]/cc);
  }
}
\end{minted}
\end{frame}

\begin{frame}[fragile]{Example (cont)}
  Rewritten to Futhark:
\begin{minted}{haskell}
let pfgk = map(\pfdv pfps pfpt pfu ->
                pfdv - (pfps - pfpt + pfu / cc)
                ) pfdv pfps pfpt pfu
\end{minted}
\end{frame}


\begin{frame}{Implementation Structures}
  \begin{itemize}
  \item C:  10 nested filter loops inside a main convergence loop
  \item CUDA:  10 filter kernel invokations + 1 error calculation loop in a main convergence loop
  \item Futhark:  10 filter \texttt{map} operations, 1 \texttt{map}/\texttt{reduce} error calculation operation in a main convergence loop
  \item $\Rightarrow$ Compiled Futhark:  compiler fuses code into 6 operations
  \end{itemize}
\end{frame}
  
\section{Measurements}
\begin{frame}{Cameraman (238x238) image: futhark-opencl vs. CUDA}
%  Cameraman image: 238 x 238 pixels
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../Report/plots/fut-opencl-ref-opencl-cameraman-speedup.pdf}
%%    \caption{Comparison between at native OpenCL and the CUDA version of the code
%%      for cameraman image and iteration counts 150, 300 and 600}
    \label{fig:cam-ocl}
  \end{figure}
  $\Rightarrow$ $\sim$2x speedup at all iteration counts
  
  Mainly due to the lower number of kernels in the Futhark-generated code.  Still significant overhead in convergence loop double buffering
\end{frame}

\begin{frame}{Hip Joint (960x720) image: futhark-opencl vs. CUDA}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../Report/plots/fut-opencl-ref-opencl-scanimg-speedup.pdf}
%%    \caption{Comparison between at native OpenCL and the CUDA version of the code
%%      for hip scan image and iteration counts 150, 300 and 600}
    \label{fig:scan-ocl}
  \end{figure}  
  $\Rightarrow$ 4.7 to 5x speedup.  Performance advantage of Futhark  grows with iteration count

  Futhark's code is more compute intensive than the handwritten CUDA code
\end{frame}

\begin{frame}{Cameraman image: futhark-c vs. Hand-written C}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../Report/plots/fut-c-ref-c-cameraman-speedup.pdf}
    %%  \caption{Comparisons between a native C implementation and the futhark-c
    %%    versions of the code for the cameraman image and iteration counts 150, 300
    %%    and 600}
    \label{fig:cam-c}
  \end{figure}
  $\Rightarrow$ $\sim$1.7x performance \emph{drop}
%%
%%  Simpler structure of handwritten C code lends itself better to \texttt{gcc}
%%  optimizations  
\end{frame}

\begin{frame}{Hip Joint image: futhark-c vs. Hand-written C}
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../Report/plots/fut-c-ref-c-scanimg-speedup.pdf}
%%    \caption{Comparisons between a native C implementation and the futhark-c
%%      versions of the code for the hip joint scan image and iteration counts 150,
%%      300 and 600. Notice the y-axis unit.}
    \label{fig:scan-c}
  \end{figure}
  $\Rightarrow$ $\sim$ 1.4x slowdown.  \texttt{futhark-c} performance approaches native C as image size increases
\end{frame}

\begin{frame}{Optimization opportunity}
  Futhark does double buffering between loop iterations. Result of an iteration
  is copied to separate buffers which are given as input to the following iteration.

  This double buffering is avoidable and carries a performance
  overhead.

  Manual loop unrolling shows that quite significant performance improvements are
  possible by eliminating this overhead.
\end{frame}

\begin{frame}[fragile]{Main Loop, no unrolls}
  \begin{minted}{haskell}
let (<vars>) =
  loop (<vars>) =
    while err > errorbound && i < iter_max do
      let (<output_vars>) =
          unsafe CMF (<cmf_vars>) in
      (<output_vars>)
  \end{minted}
\end{frame}

\begin{frame}[fragile]{Main Loop, one unroll}
  \begin{minted}{haskell}
let (<vars>) =
  loop (<vars>) =
     while err > errorbound && i < iter_max do
       let (<output_vars>) =
            unsafe CMF (<cmf_vars>) in
              let (output_vars2>) = 
                   unsafe CMF (<output_vars>) in
              (<output_vars2>)
  \end{minted}
\end{frame}

\begin{frame}{Manual loop unrolling: futhark-opencl, Cameraman image}
%%  Cameraman image: 238 x 238 pixels
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../Report/plots/loop-unroll-opencl-speedup.pdf}
    %%  \caption{Performance gains achieved when applying partial loop unrolling to
    %%    futhark-opencl. Measurements shown for 300 iterations on the cameraman image.}
    \label{fig:unroll-opencl}
  \end{figure}
  $\Rightarrow$ Almost 1.8x \emph{additional} speedup can be achieved

  The overhead of memory copy operations in Futhark's double buffering is significant
\end{frame}


\begin{frame}{Manual loop unrolling: futhark-c, Cameraman image}
%  Cameraman image:  238 x 238 pixels
  \begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{../Report/plots/loop-unroll-c-speedup.pdf}
    %%  \caption{Performance gains achieved when applying partial loop unrolling to
    %%    futhark-c. Measurements shown for 300 iterations of the cameraman image.}
    \label{fig:unroll-c}
  \end{figure}
  $\Rightarrow$ modest gains for C code loop unrolling

  Memory copy in Futhark's C code accounts for on the order of 10\% of execution time.  Profiling
  confirms this
\end{frame}

\begin{frame}{Rapid prototyping graphical interface}
  Shows the output of the algorithm for every iteration. Gives immediate
  feedback to algorithm developer 

  Demonstrates key advantage of Futhark: The same code used for prototyping is
  later put into production on massively parallel hardware (No Matlab
  $\rightarrow$ CUDA porting)

  Developed in Python/PyQT. Uses cffi (Foreign Function Interface) for to call a
  C wrpper library around the Futhark C-API.

  Straight-forward development process
\end{frame}

\begin{frame}{Live Demo!}
  \begin{figure}
  \begin{center}
  \includegraphics[width=.75\textwidth]{../Report/images/GUI_Screenshot}
  % \caption{Screenshot of the \emph{CMF Visualizer} rapid prototyping tool.  The input fields
  %   let the user change the parameters that are passed to the algorithm, and the render button
  %   executed the calculations.  Images from all the resulting iterations can be viewed by
  %   scrolling through them using the slider at the bottom.}
  % \label{fig:screenshot}
  \end{center}
\end{figure}

\end{frame}

\begin{frame}{Conclusions}
  \begin{itemize}
  \item Futhark delivers impressive performance and significantly outperforms
    hand-written CUDA
  \item Real-time performance may be achievable depending on required
    segmentation quality. With default parameters, we're only half the way.
  \item Elimination of double-buffering in Futhark will further improve
    performance
  \item Demonstration of the flexibility of the multiple-backend model of
    Futhark and the usability of the API exposed by the generated code.
  \end{itemize}
\end{frame}
\begin{frame}[standout]
  Thank You!
\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-engine: xetex
%%% TeX-command-extra-options: "-enable-write18 -8bit"
%%% End:
