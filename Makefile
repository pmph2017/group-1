all: compile run compare

compile:
	futhark-c CMF.fut

run:
	img2fut/img2fut.py images/cameraman.jpg | ./CMF -t /dev/stderr | img2fut/fut2img.py output.png

compare:
	compare -compose src cameraman_reference.png output.png difference.png
	eog difference.png

makedata:
