/***************************************************************************/
/*      Name:       CMF_Mex.c

        Performing the continuous max-flow algorithm to solve the
        continuous min-cut problem in 2D

        Usage: [u, erriter, i, timet] = CMF_Mex(penalty, C_s, C_t, para);

        Inputs (penalty, C_s, C_t, para):

               - penalty: point to the edge-weight penalty parameters to
                          total-variation function.

                 For the case without incorporating image-edge weights,
                 penalty is given by the constant everywhere. For the case
                 with image-edge weights, penalty is given by the pixelwise
                 weight function:

                 for example, penalty(x) = b/(1 + a*| grad f(x)|) where b,a > 0 .

               - C_s: point to the capacities of source flows ps

               - C_t: point to the capacities of sink flows pt

               - para: a sequence of parameters for the algorithm
                    para[0,1]: rows, cols of the given image
                    para[2]: the maximum iteration number
                    para[3]: the error bound for convergence
                    para[4]: cc for the step-size of augmented Lagrangian method
                    para[5]: the step-size for the graident-projection step to the
                           total-variation function. Its optimal range is [0.1, 0.17].

        Outputs (u, erriter, i, timet):

              - u: the final results u(x) in [0,1]. As the following paper,
                   the global binary result can be available by threshholding u
                  by any constant alpha in (0,1):

                  Nikolova, M.; Esedoglu, S.; Chan, T. F.
                  Algorithms for Finding Global Minimizers of Image Segmentation and Denoising Models
                  SIAM J. App. Math., 2006, 66, 1632-1648

               - erriter: it returns the error evaluation of each iteration,
                  i.e. it shows the convergence rate. One can check the algorithm
                  performance.

               - i: gives the total number of iterations, when the algorithm converges.

               - timet: gives the total computation time.

           Compile:

               >> mex CMF_Mex.c

           Example:

               >> [u, erriter, i, timet] = CMF_Mex(single(penalty), single(Cs), single(Ct), single(para));

               >> us = max(u, beta);  % where beta in (0,1)

               >> imagesc(us), colormap gray, axis image, axis off;figure(gcf)

               >> figure, loglog(erriter,'DisplayName','erriterN');figure(gcf)


       The original continuous max-flow algorithm was proposed in the following papers:

       [1] Yuan, J.; Bae, E.;  Tai, X.-C.
           A Study on Continuous Max-Flow and Min-Cut Approaches
           CVPR, 2010

       [2] Yuan, J.; Bae, E.; Tai, X.-C.; Boycov, Y.
           A study on continuous max-flow and min-cut approaches. Part I: Binary labeling
           UCLA CAM, Technical Report 10-61, 2010

       The mimetic finite-difference discretization method was proposed for
       the total-variation function in the paper:

       [1] Yuan, J.; Schn{\"o}rr, C.; Steidl, G.
           Simultaneous Optical Flow Estimation and Decomposition
           SIAM J.~Scientific Computing, 2007, vol. 29, page 2283-2304, number 6

       This software can be used only for research purposes, you should cite ALL of
       the aforementioned papers in any resulting publication.

       Please email Jing Yuan (cn.yuanjing@gmail.com) for any questions, suggestions and bug reports

       The Software is provided "as is", without warranty of any kind.


                           Version 1.0
               https://sites.google.com/site/wwwjingyuan/

               Copyright 2011 Jing Yuan (cn.yuanjing@gmail.com)

*/
/***************************************************************************/




/*  compilation command (under matlab): mex CMF_mex.c  */

#include <stdio.h>
#include <stdlib.h>
//#include <mex.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>

#ifndef INDATA
#define INDATA "indata.h"
#endif
#include INDATA

#define YES 0
#define NO 1

#define PI 3.1415926

#define MAX(a,b) ( a > b ? a : b )
#define MIN(a,b) ( a <= b ? a : b )
#define SIGN(x) ( x >= 0.0 ? 1.0 : -1.0 )
#define ABS(x) ( (x) > 0.0 ? x : -(x) )
#define SQR(x) (x)*(x)

#ifndef HAVE_RINT
#define rint(A) floor((A)+(((A) < 0)? -0.5 : 0.5))
#endif

float SQRT(float number) {
    /* long i; */
    /* float x, y; */
    /* const float f = 1.5F; */

    /* x = number * 0.5F; */
    /* y  = number; */
    /* i  = * ( long * ) &y; */
    /* i  = 0x5f3759df - ( i >> 1 ); */
    /* y  = * ( float * ) &i; */
    /* y  = y * ( f - ( x * y * y ) ); */
    /* y  = y * ( f - ( x * y * y ) ); */
    /* return -(number * y); */
    return sqrt(number);
}


static int64_t get_wall_time() {
  struct timeval time;
  gettimeofday(&time,NULL);
  return time.tv_sec * 1000000 + time.tv_usec;
}

/**********************************************/
/************** MAIN FUNCTION *****************/
/**********************************************/

/****************************************/
//void main(int iNbOut, mxArray *pmxOut[],
//int iNbIn, const mxArray *pmxIn[])
int main( void )
{

  /* iNbOut: number of outputs
     pmxOut: array of pointers to output arguments */

  /* iNbIn: number of inputs
     pmxIn: array of pointers to input arguments */

  /* float* pfpenalty; */
  /* float* pfCs; */
  /* float* pfCt; */
  float    *pfu, *pfcvg, *pfVecParameters;
  float   *pfbx, *pfby, *pfps, *pfpt, *pfgk, *pfdv;
  float   fLambda, fError, cc, steps;
  float   fpt, fps;
  int     punum, iNy, iNx, iNdim, ix, iy, i, iNI;
  int  iDim[3];
  int     iNbIters, szImg, idx, index;
  int64_t  start_time, end_time, tt;

  /* Inputs */
  const float* pfpenalty = &penalty[0];
  const float* pfCs = &fCs[0];
  const float* pfCt = &fCt[0];

  /* Size */
  iNy = height;
  iNx = width;
  szImg = iNy * iNx;

  /* Choice of region segmentation model */
  iNbIters = iterations;
  fError = errorbound;
  cc = cc_stepsize;
  steps = gradstep;


  /* Outputs */
  /* outputs the computed u(x)  */
  iNdim = 2;
  iDim[0] = iNy;
  iDim[1] = iNx;

  pfu = (float*) calloc(iNy * iNx, sizeof(float));

  /* outputs the convergence rate  */
  pfcvg = (float*) calloc(iNbIters, sizeof(float));

  /* Memory allocation */

  /* allocate the memory for px */
  pfbx = (float *) calloc( (unsigned)(iNy*(iNx+1)), sizeof(float) );
  if (!pfbx)
    printf("Memory allocation failure\n");

  /* allocate the memory for py */
  pfby = (float *) calloc( (unsigned)((iNy+1)*iNx), sizeof(float) );
  if (!pfby)
    printf("Memory allocation failure\n");

  /* allocate the memory for ps */
  pfps = (float *) calloc( (unsigned)(iNy*iNx), sizeof(float) );
  if (!pfps)
    printf("Memory allocation failure\n");

  /* allocate the memory for pt */
  pfpt = (float *) calloc( (unsigned)(iNy*iNx), sizeof(float) );
  if (!pfpt)
    printf("Memory allocation failure\n");

  /* allocate the memory for gk */
  pfgk = (float *) calloc( (unsigned)(iNy*iNx), sizeof(float) );
  if (!pfgk)
    printf("Memory allocation failure\n");

  /* allocate the memory for div */
  pfdv = (float *) calloc( (unsigned)(iNy*iNx), sizeof(float) );
  if (!pfdv)
    printf("Memory allocation failure\n");

  /* Preparing initial values for flows and u */
  for (ix=0; ix< iNx; ix++){
    idx = ix*iNy;
    for (iy=0; iy< iNy; iy++){
      index = idx + iy;
      if (pfCs[index] < pfCt[index]){
        pfps[index] = pfCs[index];
        pfpt[index] = pfCs[index];
        pfdv[index] = pfbx[index+iNy] - pfbx[index]
          + pfby[index+1] - pfby[index];
      }
      else{
        pfu[index] = 1;
        pfps[index] = pfCt[index];
        pfpt[index] = pfCt[index];
        pfdv[index] = pfbx[index+iNy] - pfbx[index]
          + pfby[index+1] - pfby[index];
      }
    }
  }
  /*  Main iterations */

  iNI = 0;

  start_time = get_wall_time();

  while( iNI<iNbIters )
    {

      /* update p */

      for (ix=0; ix< iNx; ix++){
        idx = ix*iNy;
        for (iy=0; iy< iNy; iy++){
          index = idx + iy;
          pfgk[index] = pfdv[index] - (pfps[index] - pfpt[index]
                                       + pfu[index]/cc);

        }
      }

      /* update px */
      // Look at this
      for (ix=0; ix < iNx-1; ix++){
        idx = (ix+1)*iNy;
        for (iy=0; iy < iNy; iy++){
          index = idx + iy;
          pfbx[index] = steps*(pfgk[index] - pfgk[index-iNy]) + pfbx[index];
        }
      }

      /* update py */

      for(ix = 0; ix < iNx; ix ++){
        idx = ix*iNy;
        for(iy = 0; iy < iNy-1; iy ++){
          index = idx + iy + 1;
          pfby[index] = steps*(pfgk[index] - pfgk[index-1]) + pfby[index];
        }
      }

      /* projection step */

      for (ix = 0; ix < iNx; ix++){
        idx = ix*iNy;
        for (iy = 0; iy < iNy; iy++){
          index = idx + iy;
          fpt = SQRT((SQR(pfbx[index]) + SQR(pfbx[index+iNy])
                      + SQR(pfby[index]) + SQR(pfby[index+1]))*0.5);

          if (fpt > pfpenalty[index])
            fpt = fpt / pfpenalty[index];
          else
            fpt = 1;

          pfgk[index] = 1/fpt;
        }
      }

      for (ix =0; ix < iNx-1; ix++){
        idx = (ix+1)*iNy;
        for (iy = 0; iy < iNy; iy++){
          index = idx + iy;
          pfbx[index] = (pfgk[index] + pfgk[index-iNy])*0.5*pfbx[index];
        }
      }

      for (ix = 0; ix < iNx; ix++){
        idx = ix*iNy;
        for (iy = 0; iy < iNy-1; iy++){
          index = idx + iy + 1;
          pfby[index] = (pfgk[index-1] + pfgk[index])*0.5*pfby[index];
        }
      }

      /* compute the divergence  */

      for (ix = 0; ix < iNx; ix++){
        idx = ix*iNy;
        for (iy = 0; iy < iNy; iy++){
          index = idx + iy;
          pfdv[index] = pfbx[index+iNy] - pfbx[index]
            + pfby[index+1] - pfby[index];
        }
      }

      /* update ps  */

      for (ix = 0; ix < iNx; ix++){
        idx = ix*iNy;
        for (iy = 0; iy < iNy; iy++){
          index = idx + iy;
          fpt = pfpt[index] - pfu[index]/cc + pfdv[index] + 1/cc;
          fpt = MIN(fpt , pfCs[index]);
          pfps[index] = fpt;
        }
      }

      /* update pt  */

      for (ix = 0; ix < iNx; ix++){
        idx = ix*iNy;
        for (iy = 0; iy < iNy; iy++){
          index = idx + iy;
          fpt = pfps[index] + pfu[index]/cc - pfdv[index];
          fpt = MIN(fpt , pfCt[index]);
          pfpt[index] = fpt;
        }
      }

      /* update multipliers */

      fps = 0;
      for (ix = 0; ix < iNx; ix++){
        idx = ix*iNy;
        for (iy = 0; iy < iNy; iy++){
          index = idx + iy;
          fpt = cc*(pfpt[index] + pfdv[index] - pfps[index]);
          fps += ABS(fpt);

          pfu[index] -= fpt;
        }
      }

      pfcvg[iNI] = fps / szImg;

      if ((pfcvg[iNI] <= fError) && (iNI >= 2))
        break;

      iNI ++;
    }

  fprintf(stderr, "Total iteration number = %i\n",iNI);
  fprintf(stderr, "Final error = %f\n", fError);

  /* Outputs (see above) */

  punum = iNI;

  /* Free memory */

  free( (float *) pfbx );
  free( (float *) pfby );
  free( (float *) pfps );
  free( (float *) pfpt );
  free( (float *) pfgk );
  free( (float *) pfdv );

  end_time = get_wall_time();
  tt = end_time - start_time;

  fprintf(stderr, "\nComputing Time for max-flow = %d us\n \n", tt);

  //iNy = 10;
  //iNx = 10;
  printf("%d\n%f\n", iNI, pfcvg[iNI]);
  fprintf(stdout, "[");
  for (int i = 0; i < iNy; i++) {
    fprintf(stdout, "[");
    int j;
    for (j = 0; j < iNx - 1; j++) {
      fprintf(stdout, "%ff32, ", pfu[i * iNy + j]);
    }
    if (i == iNy - 1) {
      fprintf(stdout, "%ff32]", pfu[i * iNy + (++j)]);
    } else {
      fprintf(stdout, "%ff32], ", pfu[i * iNy + (++j)]);
    }
    }
  fprintf(stdout, "]\n");

  //printf("%f\n", SQRT(9.0));

}
/****************************************/
