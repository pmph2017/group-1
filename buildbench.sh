#!/bin/sh

set -ex

# Generate unrolled futhark programs
for n in `seq 2 5`; do
    sed -e"s/_CMF_UNROLL_SIZE_/$n/g" CMF.fut.in > CMF-unroll-$n.fut
done

# Build hand-written CUDA and C-programs
for f in datasets/*.h; do
    nvcc CMF-orig-cuda.cu -DINDATA=\"${f}\" -o CMF-orig-cuda_$(basename ${f%.h})
    gcc -O3 -std=c11 CMF-seq.c -lm -DINDATA=\"${f}\" -o CMF-seq_$(basename ${f%.h})
done
