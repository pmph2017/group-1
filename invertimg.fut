
let invert (img : [][]f32) : [][]f32 =
  let inv = map(\ys -> 
		map(\x -> 1.0f32-x) ys) img in
  inv

let main (img : [i][j]f32) : [][]f32 = invert img