\section{Analysis}
\label{sec:analysis}
In this section, we discuss the structure of the \emph{\CMF} algorithm that we are examining, and
what opportunities it presents for parallelization optimizations.

\subsection{Background}
\emph{\CMF} is an image segmentation algorithm presented by Jing Yuan et al in \cite{yuan}. \\
It works on an input image and, over several iterations, erodes away disjointed and
indistics features, leaving strongly-connected and clear features in the output.\\
\begin{figure}[!h]
  \includegraphics[width=.45\textwidth]{images/cameraman} \includegraphics[width=0.45\textwidth]{images/Cameraman_Segmented}
  \caption{Reference image, before (left) and after (right) segmentation with the \CMF\ algorithm}
  \label{fig:refimages}
\end{figure}
Figure \ref{fig:refimages} shows a sample input image and the resulting output image after 36
iterations of the \CMF\ algorithm.\\
Our goal of the project is to determine whether an implementation of \CMF\ optimized
for parallel hardware will be fast enough for use in real-time medical imaging devices
such as ultrasound scanners.  The algorithm has previously been deemed to be of interest
by medical imaging specialists for use in locating and isolating features of interest in
such images.\\
To be of interest for medical imaging, the parallel \CMF\ algorithm should be fast enough
to process images in real-time (60 frames per second) on contemporary hardware. \\
A secondary objective of this project is to compare how well an implementation of the \CMF\
algorithm that is written in a high-level language (Futhark) and compiled to a parallel backend
(OpenCL) compares to a hand-written parallel implementation.
If the performance of the machine-generated parallel code is comparable to, or exceeds,
that of the hand-written version, multiple benefits can be had, most importantly:
\begin{itemize}
\item Easier-to-maintain high-level code that performs as well as hand-crafted
  hardware-near code
\item One high-level codebase can compile to multiple lowlevel backends, which allows
  users to work with the code with a variety of tools
\item Cost of development in high-level languages is lower than in low-level languages
\item Improvements made to the high-level language compiler will benefit all algorithms
  implemented in the high-level language by simple recompilation
\end{itemize}


\subsection{Reference Material}
Yuan has made public two implementations of the \CMF\ algorithm:  a sequential version
implemented i C, and a parallel version implemented in CUDA.  We will make use of these
implementations both to verify correctness of our own implementation, and to compare the
speed of execution.  Jing Yuan's software is available from his website at
\url{https://sites.google.com/site/wwwjingyuan/continuous-max-flow}.

\subsection{Problem Structure}
The \CMF\ algorithm works on 2-dimensional images\footnote{A 3-dimensional version also exists, but for
  our purposes, we focus on the lower-dimensional one.} and in a series of iterations,
it runs a collection of filters over the whole image.  It does so until the image that
is produced at the end of an iteration is sufficiently little different from the
input image at the start of the iterations (convergence), or until a pre-defined maximum
number of iterations have been executed.\\
For each iteration, 10 2-dimensional filters are updated with values from all of the image
or other filters.  At the end of the iteration, an error value (the difference from the image at
the beginning of the iteration) is computed by subtracting the two images from each
other.\\
Thus, the \CMF\ algorithm is structured as a main loop (convergence loop) with ten filter
loops embedded within (the error calculation is fused into the last filter loop).
Each filter loop is two nested loops --- one that iterates over the columns of the image,
and one that iterates over the rows.

\subsection{Opportunities for Parallelization}
The convergence loop itself it not parallel in nature.  Every iteration of the loop depends
on the values produced in the previous iteration.  Furthermore, each time the loop has executed,
the error and iteration count must be evaluated in order to determine whether another iteration
needs to be executed.\\
Each of the ten filter loops within the convergence loop, however, are easily parallelized.  They all
loop over each of the pixels in the image and calculate an output value for that particular point.
The output value depends only on values calculated in the previous iteration of the convergence loop,
and thus the value at each pixel can be computed independently of the value at all other pixels.
In Futhark, this corresponds to a simple \verb|map| operation over the entire image.\\
That leaves the computation of the convergence criteria --- the error of the current iteration.
That value is computed as a sum of the differenced of the current image and the previous image.  The
difference operation is a \verb|map| over the images, and the sum operation is a \verb|reduce| over
the result of the difference operation.  The \verb|map| operation is highly parallel, while 
\verb|reduce| is an operation of depth $\Theta (lg\ n)$ and work $\Theta (n)$.\\
Not all of the filter \verb|map| operations are dependent on the previous \verb|map|s in the same
convergence iteration, and thus some may be fused inside a single \verb|map|.  We examine the
structure of the code generated by the Futhark compiler in Section \ref{sec:measurements}.

\subsection{Expected Results}
The reference code provided by Yuan contains two \CMF\ implementations:  a sequential C implementation,
and a parallel CUDA implementation.  The excercise of translating the \CMF\ algorithm into the Futhark
language, is basically one of helping the compiler understand the building blocks of the algorithms,
and which of these blocks may be executed in parallel.  An inspection of the reference CUDA code
reveals that it employs code optimization techniques such as minimization of memory copy and memory
access strategy (coalesced memory access).  It does not, however, apply loop or kernel fusing, and
therefore we expect the parallel OpenCL code generated by the Futhark compiler to exhibit better
performance than the CUDA code.\\
With regards to the sequential C implementation, we expect the Futhark-generated sequential code
to exhibit similar performance to the hand-crafted reference implementation.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "Report"
%%% TeX-command-extra-options: "-enable-write18"
%%% End:
