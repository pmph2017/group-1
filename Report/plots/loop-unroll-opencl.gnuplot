set datafile separator ","
set xlabel 'Unrolling level'
set ylabel 'Time (us)'
#set xrange [0:600]
#set yrange [0:90000]
set terminal pdf enhanced
set grid
show grid
#set key bottom right
set key off
set xtics 1

set output 'loop-unroll-opencl.pdf'
plot 'loop-unroll-opencl.csv' using 1:2 with linespoints title 'Futhark-opencl'

# using 2 with linespoints linewidth 2 linecolor rgb "#0000FF" title 'Copy', 'memory_bandw' using 3 with linespoints linewidth 2 linecolor rgb "#FF0000" title 'Scale'


stats 'loop-unroll-opencl.csv' every ::1::1 using 2 nooutput
value = STATS_min

set output 'loop-unroll-opencl-speedup.pdf'
set yrange [0.9:2]
set ylabel 'Speedup'
plot 'loop-unroll-opencl.csv' using 1:(value)/($2) with linespoints title 'Futhark-opencl'
