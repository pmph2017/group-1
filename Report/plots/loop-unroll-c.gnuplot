set datafile separator ","
set xlabel 'Unrolling level'
set ylabel 'Time (us)'
#set xrange [0:600]
#set yrange [0:90000]
set terminal pdf
set output 'loop-unroll-c.pdf'
set grid
show grid
set xtics 1
#set key bottom right
set key off

plot 'loop-unroll-c.csv' using 1:2 with linespoints title 'Futhark-c'

# using 2 with linespoints linewidth 2 linecolor rgb "#0000FF" title 'Copy', 'memory_bandw' using 3 with linespoints linewidth 2 linecolor rgb "#FF0000" title 'Scale'


stats 'loop-unroll-c.csv' every ::1::1 using 2 nooutput
value = STATS_min

set output 'loop-unroll-c-speedup.pdf'
set yrange [0.9:1.2]
set ylabel 'Speedup'
plot 'loop-unroll-c.csv' using 1:(value)/($2) with linespoints title 'Futhark-c'
