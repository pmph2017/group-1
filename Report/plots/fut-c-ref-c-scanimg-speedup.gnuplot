set datafile separator ","
set xlabel 'Iterations'
set ylabel 'Speedup'
#set xrange [0:600]
#set yrange [0:90000]
set terminal pdf
set output 'fut-c-ref-c-scanimg-speedup.pdf'
set grid
show grid
# set key bottom right
set key off


plot 'fut-c-ref-c-scanimg.csv' using 1:($2/$3) with linespoints title 'Speedup'

# using 2 with linespoints linewidth 2 linecolor rgb "#0000FF" title 'Copy', 'memory_bandw' using 3 with linespoints linewidth 2 linecolor rgb "#FF0000" title 'Scale'
