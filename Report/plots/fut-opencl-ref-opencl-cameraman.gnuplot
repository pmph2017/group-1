set datafile separator ","
set xlabel 'Iterations'
set ylabel 'Time (us)'
#set xrange [0:600]
#set yrange [0:90000]
set terminal pdf
set output 'fut-opencl-ref-opencl-cameraman.pdf'
set grid
show grid
set key bottom right

plot 'fut-opencl-ref-opencl-cameraman.csv' using 1:2 with linespoints title 'Manual CUDA', '' using 1:3 with linespoints title 'futhark-opencl'

# using 2 with linespoints linewidth 2 linecolor rgb "#0000FF" title 'Copy', 'memory_bandw' using 3 with linespoints linewidth 2 linecolor rgb "#FF0000" title 'Scale'
