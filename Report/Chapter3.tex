\section{Measurements}
\label{sec:measurements}
In this part of the report, we will describe the measurements that we have
performed of our Futhark implementation and compare it to preexisting C and
CUDA implementations of the image segmentation algorithm.
%We will furthermore 

All performance measurements has been performed on the gpu-01 machine, which
exhibited the best absolute GPU performance. The machine was quite busy at the
time when the benchmarks were performed and thus, some benchmarks showed quite a
bit of variance. We therefore report the lowest measured timing out of 10
repetitions. In previous benchmark runs, performed while the GPU machine were
less busy, performance between runs was very stable. We therefore believe that
these measurements accurately reflect the expected performance of the various
implementations.

%How are the results, and how do they compare to what we expected?\\

\subsection{Hand-written CUDA vs. futhark-opencl}
We first examine performance of the hand-written CUDA implementation of the
algorithm compared to the OpenCL code generated by Futhark for the reference
image and the hip joint image (\Cref{fig:cam-ocl} and \Cref{fig:scan-ocl}).

It is obvious that the performance of the OpenCL code generated by futhark is
vastly superior to the hand-written CUDA code. We further see that the CUDA code
scales worse with increasing problem sizes compared to the Futhark generated
code. This clearly highlights the importance of the optimized memory access
patterns resulting from the loop fusion performed by Futhark.

Most of the time, it is possible to beat an optimizing compiler with
hand-written code. However, writing such optimized code requires significant
skill and time. It is therefore highly impressive that Futhark is able to beat
an ``average'' hand-written implementation by such a wide margin. Especially
considering that the Futhark code is simpler, and provides a more clear
representation of the algorithm than the CUDA code.

There is still, however, room for improvements in the Futhark generated OpenCL
code by reducing the amount of memory copying performed between iterations due
to the double-buffering employed by Futhark. We later show the significance of
this overhead.

%Where are the bottlenecks and what can we do to relieve them?\\

%Double buffer copy in futhark - loop unrolling
% - Do benchmarks of the effect of performing various degrees of loop unrolling

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{plots/fut-opencl-ref-opencl-cameraman.pdf}
  \caption{Comparison between at native OpenCL and the CUDA version of the code
    for cameraman image and iteration counts 150, 300 and 600}
  \label{fig:cam-ocl}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{plots/fut-opencl-ref-opencl-scanimg.pdf}
  \caption{Comparison between at native OpenCL and the CUDA version of the code
    for hip scan image and iteration counts 150, 300 and 600}
  \label{fig:scan-ocl}
\end{figure}


\subsection{Hand-written C vs. futhark-c}

Even though the performance of the generated C-code is not a focus of the
Futhark project, it is still interesting to see how it compares to a
hand-written, reasonably optimized, C-implementation. Performance tests using
the reference cameraman image and the hip joint scan image are shown in
\Cref{fig:cam-c} and \Cref{fig:scan-c}. For the smaller image, \verb|futhark-c|
is about twice as slow as the hand-written c, while for the larger image the
difference in performance becomes smaller. This indicates that the code
generated by Futhark has a larger overhead per iteration that becomes less
significant as the size of the image (and thus amount of computation per image)
increase. A significant part of this overhead likely originates in the
double-buffering performed by Futhark between loop iterations. As for the
general performance, the loop fusion performed by Futhark which yielded
significant performance improvements for the OpenCL code might actually make the
performance of the C-versions worse since the larger loops are harder for the
compiler to optimize. In particular, vectorization can be made difficult.

\begin{figure}
  \centering

  \includegraphics[width=0.8\textwidth]{plots/fut-c-ref-c-cameraman.pdf}
  \caption{Comparisons between a native C implementation and the futhark-c
    versions of the code for the cameraman image and iteration counts 150, 300
    and 600}
  \label{fig:cam-c}
\end{figure}
\begin{figure}
  \centering

  \includegraphics[width=0.8\textwidth]{plots/fut-c-ref-c-scanimg.pdf}
  \caption{Comparisons between a native C implementation and the futhark-c
    versions of the code for the hip joint scan image and iteration counts 150,
    300 and 600. Notice the y-axis unit.}
  \label{fig:scan-c}
\end{figure}

\subsection{Loop Unrolling}

Currently, Futhark does double buffering in loop constructs, meaning that the
output data of the previous loop iteration is copied to different buffers used
as input for the next. As these extra copying operations are unnecessary, their
presence is a result of limitations in Futhark. Profiling the C-code generated
by Futhark indicates that around 10\% of the total relative runtime is spent
performing \verb|memcpy| operations. We can measure the overhead generated by
these memory copies by doing manual partial unrolling of the loop in Futhark. In
theory, by performing two iterations of the algorithm, we should cut the
overhead induced by copying memory in half. We show the results for partial
unrolling of 2-5 iterations. Running sets of iterations at the same time means
that we always run up to a multiple of the unroll size and thus we may do a few
more iterations than needed. This is never a problem, as more iterations only
improve on the convergence, and the time required by the extra iterations is
more than offset by the performance gained through unrolling. We see the impact
of the loop unrolling in \Cref{fig:unroll-opencl} and \Cref{fig:unroll-c} for
\verb!futhark-opencl! and \verb!futhark-c! respectively. We see that, as
expected, we can achieve significant performance gains by performing loop
unrolling and that the gains are most significant for the OpenCL version of the
code because the memory copying operations here contributes a relatively larger
part of the total run time.

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{plots/loop-unroll-opencl.pdf}
  \caption{Performance gains achieved when applying partial loop unrolling to
    futhark-opencl. Measurements shown for 300 iterations on the cameraman image.}
  \label{fig:unroll-opencl}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{plots/loop-unroll-c.pdf}
  \caption{Performance gains achieved when applying partial loop unrolling to
    futhark-c. Measurements shown for 300 iterations of the cameraman image.}
  \label{fig:unroll-c}
\end{figure}

\subsection{Feasibility of Real-Time Image Segmentation}
One of the purposes of this project was to evaluate the feasibility of using the
image segmentation algorithm as part of a pipeline outputting processed scanner
images in real time. Our aforementioned real-time criteria, 60 frames per
second, means that we have just 16 ms available for segmenting an image. For the
reference cameraman image this is entirely possible as we are able to perform
150 iterations in 23ms and the actual time to convergence is much lower (around
40 iterations). However, the cameraman image is just $238 \times 238$ pixels and
actual images produced by an ultrasound scanner is around $1000 \times 1000$
pixels. The dimensions of the other test image, an actual scan of a hip joint,
is $960 \times 720$ and is thus closer to the size of an actual image. Looking
at the results for that image leaves us less optimistic as 150 iterations on
this image takes around 100 ms and the number of iterations required for
convergence is usually even larger than that. Before we give up entirely we should
keep in mind that the GPUs used for these benchmarks are quite old and that
state-of-the-art hardware will therefore likely be faster. Furthermore, we may
be able to find the images in an earlier convergence state of acceptable quality
and thus increase the error threshold parameter to decrease the amount of
iterations that the algorithm runs for. Finally, we have shown that there is
still room for improvement in the code generated by Futhark, for example by
eliminating the double buffering performed between iterations.
