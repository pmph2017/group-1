set terminal pdf color enhanced
set output 'figures/ocl_vs_cuda.pdf'
set xlabel 'Iterations'
set ylabel 'Execution time (in {/Symbol m}s)'
#set xtics rotate by -90
set xtics ("300" 0.25, "1000" 1.75,)
set yrange [0:120000]
set xrange [-.5:1]
set key outside

set boxwidth 0.5
set style fill solid

set title 'Runtime on gpu01, cameraman.jpg'

plot 'ocl_vs_cuda.dat' every 2 using 1:2 with boxes title 'CUDA', \
     '' every 2::1 using 1:2 with boxes title 'futhark-opencl', \
     '' u 1:($2 + 5000.5):($2) with labels notitle

set output 'figures/c_vs_c.pdf'
set yrange [0:500000]
plot 'c_vs_c.dat' every 2 using 1:2 with boxes title 'Handcrafted C', \
     '' every 2::1 using 1:2 with boxes title 'futhark-c', \
     '' u 1:($2 + 18000.5):($2) with labels notitle
