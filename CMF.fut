-- Futhark implementation of CMF algorithm
-- ==
-- compiled input @datasets/cameraman-150.data
-- compiled input @datasets/cameraman-300.data
-- compiled input @datasets/cameraman-600.data
-- compiled input @datasets/scanimg-150.data
-- compiled input @datasets/scanimg-300.data
-- compiled input @datasets/scanimg-600.data


let sqr (x: f32) = x*x

let CMF_impl (cols : i32, rows : i32, cc : f32, gradstep : f32,
         pfdv : []f32, pfps : []f32, pfpt : []f32,  pfbx : []f32, pfby : []f32,
         penalty : []f32, pfCs : []f32, pfCt : []f32, pfu : []f32) : (f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32) =
  -- 'update p'
  let pfgk = map(\pfdv pfps pfpt pfu ->
                 pfdv - (pfps - pfpt + pfu / cc)
                ) pfdv pfps pfpt pfu

  let all_px = iota (cols * rows)                                    -- ALL the pixels

  -- 'update px'
  let pfbx_ = map(\is ->
                  if is % cols != 0 then
                    gradstep * (pfgk[is] - pfgk[is-1]) + pfbx[is]
                  else
                    pfbx[is]
                 ) all_px
  -- 'update py'
  let pfby_ = map(\is ->
                  if is >= cols then
                    gradstep * (pfgk[is] - pfgk[is-cols]) + pfby[is]
                  else
                    pfby[is]
                 ) all_px


  -- 'projection step'
  let fpt = map(\is ->
                if (is + 1) % cols != 0 && is < (cols * (rows - 1)) then
                f32.sqrt((sqr pfbx_[is] + sqr pfbx_[is + 1]
                          + sqr pfby_[is] + sqr pfby_[is + cols]) * 0.5f32)
                else
                0.0f32
               ) all_px
  let pfgk = map (\fpt penalty ->
                  1.0f32 / (if fpt > penalty then fpt / penalty else 1.0f32)
                 ) fpt penalty
  let pfbx__ = map (\pfbx is ->
                    if is % (cols) != 0 then
                      (pfgk[is] + pfgk[is - 1]) * 0.5f32 * pfbx
                    else
                      pfbx
                    ) pfbx_ all_px
  let pfby__ = map (\pfby is ->
                    if is >= cols then
                      (pfgk[is - cols] + pfgk[is]) * 0.5f32 * pfby
                    else
                      pfby
                   ) pfby_ all_px

  -- 'compute the divergence'
  let pfdv__ = map (\pfdv is ->
                    if (is + 1) % (cols) != 0 && is < (cols * (rows - 1)) then
                      pfbx__[is + 1] - pfbx__[is] + pfby__[is + cols] - pfby__[is]
                    else
                      pfdv
                    ) pfdv all_px

  -- 'update ps'
  let pfps_ = map (\pfpt pfu pfdv pfCs ->
                   f32.min (pfpt - pfu/cc + pfdv + 1.0f32/cc) pfCs
                   ) pfpt pfu pfdv__ pfCs

  -- 'update pt'
  let pfpt__ = map(\pfps pfu pfdv pfCt ->
                   f32.min (pfps + pfu/cc - pfdv) pfCt
                   ) pfps_ pfu pfdv__ pfCt

  -- 'update multipliers
  let fpt = map (\pfpt pfdv pfps ->
                 (cc * (pfpt + pfdv - pfps))
                ) pfpt__ pfdv__ pfps_

  let pfu_ = map (\fpt pfu ->
                  pfu - fpt
                 ) fpt pfu

  -- compute current error
  let fpt_abs = map(\fpt -> f32.abs(fpt)) fpt
  let err = (reduce (+) 0.0f32 fpt_abs) / f32(cols * rows) in

  (err, pfdv__, pfps_, pfpt__, pfbx__, pfby__, penalty, pfCs, pfCt, pfu_)

entry CMF (cols : i32, rows : i32, cc : f32, gradstep : f32,
           pfdv : []f32, pfps : []f32, pfpt : []f32,  pfbx : []f32, pfby : []f32,
           penalty : []f32, pfCs : []f32, pfCt : []f32, pfu : []f32) : (f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32) = unsafe (CMF_impl (cols, rows, cc, gradstep, pfdv, pfps, pfpt, pfbx, pfby, penalty, pfCs, pfCt, pfu))

-- Creates initial arryas
entry init (img_flat : []f32, rows: i32, cols: i32) : ([]f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32, []f32) =
  let pfCs = map(\p -> f32.abs(p - 0.15f32)) img_flat
  let pfCt = map(\p -> f32.abs(p - 0.6f32)) img_flat
  let penalty = replicate (cols * rows) 0.5f32

  let pfu = map(\pfCs pfCt img ->
                if pfCs < pfCt then img else 1.0f32) pfCs pfCt img_flat
  let pfbx = replicate (cols * rows) 0.0f32
  let pfby = pfbx
  let pfps = map(\pfCs pfCt -> f32.min pfCs pfCt) pfCs pfCt
  let pfpt = pfps
  let pfdv = pfbx -- all just zeroes in the calculation
  in
  (pfdv, pfps, pfpt, pfbx, pfby, penalty, pfCs, pfCt, pfu)


let main [cols][rows](iter_max : i32, errorbound : f32, cc : f32, gradstep : f32, img : [cols][rows]f32) : (i32, f32, [][]f32) =
  let img_flat = reshape (cols * rows) img

  let (pfdv, pfps, pfpt, pfbx, pfby, penalty, pfCs, pfCt, pfu) = init(img_flat, rows, cols)

  let (err, _, _, _, _, _, _, _, _, pfu, i) =
    loop (err, pfdv__, pfps__, pfpt__, pfbx__, pfby__, penalty__, pfCs__, pfCt__, pfu__, i) =
      (errorbound + 1.0f32, pfdv, pfps, pfpt, pfbx, pfby, penalty, pfCs, pfCt, pfu, 0)
        while err > errorbound && i < iter_max do
          let (err, pfdv_, pfps_, pfpt_, pfbx_, pfby_, penalty_, pfCs_, pfCt_, pfu_) =
            unsafe CMF (cols, rows, cc, gradstep, pfdv__,
                        pfps__, pfpt__, pfbx__, pfby__, penalty__, pfCs__, pfCt__, pfu__) in
          (err, pfdv_, pfps_, pfpt_, pfbx_, pfby_, penalty_, pfCs_, pfCt_, pfu_, i+1) in

    (i, err, reshape (cols, rows) pfu)
