#!/usr/bin/env python3

import numpy
import re
from scipy import misc
import argparse

def main(infile, iterations, errorbound, cc, gradstep, output_format):
    img = misc.imread(infile, mode='F')
    (height, width) = img.shape

    img = img / 255

    numpy.set_printoptions(threshold=numpy.nan)

    if output_format == 'c':
        print("const int height = %s;" % height)
        print("const int width = %s;" % width)
        print("const int iterations = %s;" % iterations)
        print("const float errorbound = %s;" % errorbound)
        print("const float cc_stepsize = %s;" % cc)
        print("const float gradstep = %s;" % gradstep)

        img_flat = img.flatten()
        penalty = numpy.ones(height * width)*0.5;
        ulab1 = 0.15
        ulab2 = 0.6
        fCs = abs(img_flat - ulab1)
        fCt = abs(img_flat - ulab2)

        print("const float fCs[] = {" + ", ".join(map(str, fCs.tolist())) + "};")
        print("const float fCt[] = {" + ", ".join(map(str, fCt.tolist())) + "};")
        print("const float penalty[] = {" + ",".join(map(str, penalty.tolist())) + "};")

    elif output_format == 'futhark':
        ies = []
        for i in img:
            js = map(lambda s: ("%ff32" % s), i)
            ies.append("[%s]" % ", ".join(js))

        print("%d %f %f %f " % (iterations, errorbound, cc, gradstep), end='')
        print("[%s]" % ", ".join(ies))
    else:
        print("Invalid output format")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert a PNG image to Futhark format')
    parser.add_argument('filename', metavar='INFILE', type=str,
                        help='The PNG file to convert')
    parser.add_argument('--iterations', metavar='INT', type=int, default=300,
                        help='Maximum number of iterations to run')
    parser.add_argument('--errorbound', metavar='FLOAT', type=float, default=0.0001,
                        help='The error bound for convergence')
    parser.add_argument('--cc', metavar='FLOAT', type=float, default=0.3,
                        help='cc for the step-size of augmented Lagrangian method')
    parser.add_argument('--gradstep', metavar='FLOAT', type=float, default=0.16,
                        help='the step-size for the graident-projection of p')
    parser.add_argument('--output', metavar='MODE', type=str, default='futhark',
                        help='output format of read images. c and futhark supported')
    args = parser.parse_args()
    main(args.filename, args.iterations, args.errorbound, args.cc, args.gradstep, args.output)
