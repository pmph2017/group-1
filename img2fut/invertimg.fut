
let invert (img : [][]f32) : [][]f32 =
  let inv = map(\ys -> 
		map(\x -> 1.0f32-x) ys) img in
  inv

let main (iter_max : i32, errorbound : f32, cc : f32, gradstep : f32, img : [][]f32) : [][]f32 = invert img