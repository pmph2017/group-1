#!/usr/bin/env python3

import numpy
from scipy import misc
import argparse
import sys

def main(outfile):
    input = sys.stdin.read()
    input = input.replace('f32', '')
    input = input.replace('i32', '')
    input = input.replace("]]\n", '')
    translator = str.maketrans('', '', '[ ');
    input = input.translate(translator)
    vars = input.split("\n")
    iters = int(vars[0])
    err = float(vars[1])
    ies = vars[2].split("],")
    js = list(map(lambda str: list(map(lambda n: float(n), str.split(','))), ies))

    height = len(ies)
    width = len(js[0])

    print("Image size:  %d x %d" %(width, height))
    print("Iterations:  %d" %iters)
    print("Final error: %f" %err)

    img = numpy.asarray(js)
    img = img * 255


    misc.imsave(outfile, img.astype(numpy.uint8))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert a 2D greyscale Futhark array to PNG.')
    parser.add_argument('filename', metavar='OUTFILE', type=str,
                        help='The file to emit PNG data to (include .png extension)')
    args = parser.parse_args()
    main(args.filename)
